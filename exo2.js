let sum = 0

for (let elm of process.argv) {
    if (!Number.isNaN(Number(elm))) {
        sum += Number(elm)
    }
}

console.log(sum)