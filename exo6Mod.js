const fs = require('fs')
const path = require('path')

const filterDir = (dirPath, extname, callback) => {
    let fileFiltred = []
    fs.readdir(dirPath, (err, dir) => {
        if (err) {
            return callback(err)
        }
        
        dir.forEach(file => {
            if (!extname || path.extname(file) == extname) {
                fileFiltred.push(file)
            }
        })

        callback(null, fileFiltred)
    })
}


module.exports = filterDir
