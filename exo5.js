const fs = require('fs')
const path = require('path')

const dirPath = process.argv[2]
const extname = process.argv[3]

fs.readdir(dirPath, (err, dir) => {
    if (err) {
        return
    }
    
    dir.forEach(file => {
        if (!extname || path.extname(file) == extname) {
            console.log(file)
        }
    })
})

