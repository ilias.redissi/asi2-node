const exo6Mod = require('./exo6Mod')

const dirPath = process.argv[2]
const extname = process.argv[3]

exo6Mod(dirPath, extname, (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    data.forEach(file => {
        console.log(file)
    })
})

